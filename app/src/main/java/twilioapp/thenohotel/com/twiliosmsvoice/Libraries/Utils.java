package twilioapp.thenohotel.com.twiliosmsvoice.Libraries;

import android.app.Notification;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Utils {
    public static String formatPhone(String phoneNumber) {
        String formattedPhoneNumber = phoneNumber.replaceAll("(\\s+|\\(|\\)|-|\\+1)","");
        int phoneLength = formattedPhoneNumber.length();
        if (phoneLength == 10) {
            formattedPhoneNumber = String.format("(%s) %s-%s",formattedPhoneNumber.substring(0,3),formattedPhoneNumber.substring(3,6),formattedPhoneNumber.substring(6,10));
        } else if (phoneLength > 10) {
            formattedPhoneNumber = String.format("%s %s",formattedPhoneNumber.substring(0,phoneLength-10),formattedPhoneNumber.substring(phoneLength-10,phoneLength));
        }
        return formattedPhoneNumber;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    public interface OnBitmapLoaded {
        Notification onBitmapLoaded(Bitmap bmp);
    }

    public static class LoadImageAsyncTask  extends AsyncTask<String, String, Bitmap> {
        public OnBitmapLoaded bmpListener;
        public LoadImageAsyncTask(OnBitmapLoaded bmpListener) {
            this.bmpListener = bmpListener;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            try {
                URL url = new URL(params[0]);
                bitmap = BitmapFactory.decodeStream((InputStream)url.getContent());
            } catch (IOException e) {
                //Log.e(TAG, e.getMessage());
            }
            return bitmap;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            bmpListener.onBitmapLoaded(bitmap);
        }
    }

    public interface OnJSONTaskCompleted {
        void onJSONTaskCompleted(JSONObject result);
    }

    public static class JSONAsyncTask extends AsyncTask<String, String, String> {
        public OnJSONTaskCompleted listener;
        public JSONAsyncTask(OnJSONTaskCompleted listener){
            this.listener = listener;
        }

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            URL url = null;
            try {
                url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                    //Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {connection.disconnect();}
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObj = new JSONObject(result);
                listener.onJSONTaskCompleted(jsonObj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
