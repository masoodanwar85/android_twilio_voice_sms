package twilioapp.thenohotel.com.twiliosmsvoice.SMS;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import twilioapp.thenohotel.com.twiliosmsvoice.Libraries.Utils;
import twilioapp.thenohotel.com.twiliosmsvoice.R;

import static twilioapp.thenohotel.com.twiliosmsvoice.Common.APIEndPoints.API_END_POINT_SEND_MESSAGE;
import static twilioapp.thenohotel.com.twiliosmsvoice.Common.APIEndPoints.API_END_POINT_THREAD_SMSes;

public class SMSDetailActivity extends AppCompatActivity {

    ArrayList<SMSSingleItemObject> threadSMSes;
    RecyclerView singleThreadSMSes;
    ImageView btnDetailSend;
    ProgressBar pbDetailMessage;
    public static String toPhoneNumber;
    public static String fromPhoneNumber;
    EditText messageToSend;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms_detail_activity);

        Intent intent = getIntent();
        String threadID = intent.getStringExtra("com.thenohotel.twilioapp.THREADID");
        String bookingID = intent.getStringExtra("com.thenohotel.twilioapp.BOOKINGID");

        if (threadID == null && bookingID == null) {
            threadID = getIntent().getExtras().get("threadID").toString();
            bookingID = getIntent().getExtras().get("bookingID").toString();
        }

        Utils.JSONAsyncTask SMSesThread = new Utils.JSONAsyncTask(new Utils.OnJSONTaskCompleted() {
            @Override
            public void onJSONTaskCompleted(JSONObject result) {
                try {
                    JSONArray cols = result.getJSONArray("DATA");
                    android.support.v7.app.ActionBar ab = getSupportActionBar();
                    threadSMSes = new ArrayList<>();
                    for (int i =0; i < cols.length(); i++) {
                        JSONArray twilioSMSThread = cols.getJSONArray(i);
                        Integer threadID = (Integer) twilioSMSThread.get(0);
                        String smsMessage = twilioSMSThread.get(14).toString();
                        String smsTimestamp = twilioSMSThread.get(17).toString();
                        String smsFrom = twilioSMSThread.get(19).toString();
                        ab.setTitle(twilioSMSThread.get(5).toString() + " " + twilioSMSThread.get(6).toString());
                        ab.setSubtitle(Html.fromHtml("<small>" + String.format("%s (%s)",twilioSMSThread.get(21).toString(),twilioSMSThread.get(22).toString()) + "</small>"));
                        toPhoneNumber = twilioSMSThread.get(9).toString().replaceAll("(\\s+|\\(|\\)|-|\\+1)","");
                        fromPhoneNumber = twilioSMSThread.get(10).toString().replaceAll("(\\s+|\\(|\\)|-|\\+1)","");
                        threadSMSes.add(new SMSSingleItemObject(smsFrom,smsMessage,smsTimestamp));
                    }
                    singleThreadSMSes.setAdapter(new SMSDetailAdapter(threadSMSes));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        SMSesThread.execute(String.format("%s?threadID=%s&bookingID=%s",API_END_POINT_THREAD_SMSes,String.valueOf(threadID),String.valueOf(bookingID)));

        btnDetailSend = findViewById(R.id.btnDetailSend);
        messageToSend = findViewById(R.id.detailMessage);
        pbDetailMessage = findViewById(R.id.pbDetailMessage);
        pbDetailMessage.getIndeterminateDrawable().setColorFilter(0xFFFF0000, android.graphics.PorterDuff.Mode.MULTIPLY);
        singleThreadSMSes = findViewById(R.id.reyclerview_message_list);
        singleThreadSMSes.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        //DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(singleThreadSMSes.getContext(),DividerItemDecoration.VERTICAL);
        //singleThreadSMSes.addItemDecoration(dividerItemDecoration);

        btnDetailSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (messageToSend.getText().toString().trim().equals("")) {
                    Toast.makeText(getApplicationContext(),"Please type the message.",Toast.LENGTH_SHORT).show();
                } else {
                    pbDetailMessage.setVisibility(View.VISIBLE);
                    String url = API_END_POINT_SEND_MESSAGE;
                    RequestQueue MyRequestQueue = Volley.newRequestQueue(getApplicationContext());
                    StringRequest MyStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //This code is executed if the server responds, whether or not the response contains data.
                            //The String 'response' contains the server's response.
                            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                            threadSMSes.add(threadSMSes.size(),new SMSSingleItemObject("Host",messageToSend.getText().toString().trim(),timestamp.toString()));
                            SMSDetailAdapter smsDetailAdapter = new SMSDetailAdapter(threadSMSes);
                            smsDetailAdapter.notifyItemInserted(threadSMSes.size()-1);
                            singleThreadSMSes.requestLayout();
                            messageToSend.setText("");
                            // Add the current message to the SMS Detail List
                            // Update the SMSListAdapter so that it shows the latest message
                            Toast.makeText(getApplicationContext(),"Message was sent successfully.",Toast.LENGTH_SHORT).show();
                            pbDetailMessage.setVisibility(View.GONE);

                        }
                    }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //This code is executed if there is an error.
                        }
                    }) {
                        protected Map<String, String> getParams() {
                            Map<String, String> MyData = new HashMap<String, String>();
                            MyData.put("toNumber",toPhoneNumber);
                            MyData.put("fromNumber",fromPhoneNumber);
                            MyData.put("message",messageToSend.getText().toString());
                            return MyData;
                        }
                    };
                    MyRequestQueue.add(MyStringRequest);

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            this.finish();
        } else {
            super.onBackPressed(); //replaced
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent parentIntent = NavUtils.getParentActivityIntent(this);
                if(parentIntent == null) {
                    finish();
                    return true;
                } else {
                    parentIntent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(parentIntent);
                    finish();
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }
}
