package twilioapp.thenohotel.com.twiliosmsvoice.Common;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.twilio.voice.CallInvite;
import com.twilio.voice.MessageException;
import com.twilio.voice.MessageListener;
import com.twilio.voice.Voice;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import twilioapp.thenohotel.com.twiliosmsvoice.Libraries.SoundPoolManager;
import twilioapp.thenohotel.com.twiliosmsvoice.R;
import twilioapp.thenohotel.com.twiliosmsvoice.SMS.AnonSMSDetailActivity;
import twilioapp.thenohotel.com.twiliosmsvoice.SMS.SMSDetailActivity;
import twilioapp.thenohotel.com.twiliosmsvoice.Voice.ActiveCallActivity;

import static twilioapp.thenohotel.com.twiliosmsvoice.Common.MainActivity.guestsBookingsInfos;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FirebaseMessageService";

    private static final String NOTIFICATION_ID_KEY = "NOTIFICATION_ID";
    private static final String CALL_SID_KEY = "CALL_SID";
    private static final String VOICE_CHANNEL = "default";
    private NotificationManager notificationManager;
    private HashMap<String,String> incomingCallParams = new HashMap<>();
    Bitmap placeholder;
    int notID;

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        placeholder = BitmapFactory.decodeResource(getResources(),R.drawable.icon_user_100);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getFrom().equals(getResources().getString(R.string.SMSSubscriptionTopic))) {
            SMSReceived(remoteMessage);
        }
    }

    public void SMSReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        Intent smsDetailIntent;
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (remoteMessage.getNotification().getClickAction().equals("twiliosmsvoice.com.thenohotel.twilioapp.SMSDetailActivity")) {
                smsDetailIntent = new Intent(getApplicationContext(), SMSDetailActivity.class);
                smsDetailIntent.putExtra("com.thenohotel.twilioapp.THREADID", remoteMessage.getData().get("threadID"));
                smsDetailIntent.putExtra("com.thenohotel.twilioapp.BOOKINGID", remoteMessage.getData().get("bookingID"));
            } else if(remoteMessage.getNotification().getClickAction().equals("twiliosmsvoice.com.thenohotel.twilioapp.AnonSMSDetailActivity")) {
                smsDetailIntent = new Intent(getApplicationContext(), AnonSMSDetailActivity.class);
                smsDetailIntent.putExtra("com.thenohotel.twilioapp.hostNumber", remoteMessage.getData().get("hostNumber"));
                smsDetailIntent.putExtra("com.thenohotel.twilioapp.guestNumber", remoteMessage.getData().get("guestNumber"));
                smsDetailIntent.putExtra("com.thenohotel.twilioapp.BOOKINGID", remoteMessage.getData().get("bookingID"));

            } else {
                smsDetailIntent = new Intent(getApplicationContext(),MainActivity.class);
            }

        } else {
            smsDetailIntent = new Intent(getApplicationContext(),MainActivity.class);
        }

        smsDetailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(smsDetailIntent);
        PendingIntent pi = PendingIntent.getActivity(this, 0, smsDetailIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"default")
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody())
                .setAutoCancel(true)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pi);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId("com.myApp");
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    "com.myApp",
                    "My App",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
        notificationManager.notify(2,builder.build());
    }


    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
    }

}