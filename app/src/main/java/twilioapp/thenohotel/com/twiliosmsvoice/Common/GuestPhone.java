package twilioapp.thenohotel.com.twiliosmsvoice.Common;

public class GuestPhone {
    private String text;
    private String value;
    private String guestName;
    private String hostName;
    private String hostTwilioPhone;
    private int accountID;

    public GuestPhone(String text, String value, String guestName, String hostName, String hostTwilioPhone,int accountID) {
        this.text = text;
        this.value = value;
        this.guestName = guestName;
        this.hostName = hostName;
        this.hostTwilioPhone = hostTwilioPhone;
        this.accountID = accountID;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getHostName() {return hostName;}

    public String getHostTwilioPhone() {return hostTwilioPhone;}

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }
}

