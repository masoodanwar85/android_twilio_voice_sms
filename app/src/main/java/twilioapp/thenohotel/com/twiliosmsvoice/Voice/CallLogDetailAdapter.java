package twilioapp.thenohotel.com.twiliosmsvoice.Voice;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import twilioapp.thenohotel.com.twiliosmsvoice.R;

public class CallLogDetailAdapter extends RecyclerView.Adapter<CallLogDetailAdapter.CallLogDetailViewHolder> {
    private ArrayList<CallLogDetailsObject> callLogsData;

    public CallLogDetailAdapter(ArrayList<CallLogDetailsObject> callLogsData) {
        this.callLogsData = callLogsData;
    }

    @NonNull
    @Override
    public CallLogDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.call_log_detail_item,parent,false);
        return new CallLogDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CallLogDetailViewHolder holder, int position) {
        CallLogDetailsObject callLogDetailObj = callLogsData.get(position);
        CallLogDetailViewHolder callLogDetailViewHolder = holder;
        callLogDetailViewHolder.txtTimeStamp.setText(callLogDetailObj.getDateCreated());
        callLogDetailViewHolder.txtCallDuration.setText(callLogDetailObj.getDateCreated());
    }

    @Override
    public int getItemCount() {
        return callLogsData.size();
    }

    public class CallLogDetailViewHolder extends RecyclerView.ViewHolder {
        TextView txtTimeStamp;
        TextView txtCallDuration;

        public CallLogDetailViewHolder(View itemView) {
            super(itemView);
            txtTimeStamp = itemView.findViewById(R.id.callLogDetailTimeStamp);
            txtCallDuration = itemView.findViewById(R.id.detailCallDuration);
        }
    }
}