package twilioapp.thenohotel.com.twiliosmsvoice.Voice;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import twilioapp.thenohotel.com.twiliosmsvoice.Libraries.Utils;
import twilioapp.thenohotel.com.twiliosmsvoice.R;
import twilioapp.thenohotel.com.twiliosmsvoice.SMS.SMSSingleItemObject;

import static twilioapp.thenohotel.com.twiliosmsvoice.Common.APIEndPoints.API_END_POINT_CALL_LOG_DETAIL;

public class CallLogDetailActivity extends AppCompatActivity {
    ArrayList<CallLogDetailsObject> callLogDetailsObject;
    RecyclerView recyclerViewCallDetail;
    //ProgressBar pbDetailMessage;
    TextView txtHostName;
    TextView txtGuestName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_log_detail);

        Intent intent = getIntent();
        String fromPhoneNumber = intent.getStringExtra("com.thenohotel.twilioapp.voice.FROM_PHONE");
        String toPhoneNumber = intent.getStringExtra("com.thenohotel.twilioapp.voice.TO_PHONE");
        String fromToPhoneNumber = intent.getStringExtra("com.thenohotel.twilioapp.voice.FROM_TO_PHONE");
        String hostName = intent.getStringExtra("com.thenohotel.twilioapp.voice.HOST_NAME");
        String guestName = intent.getStringExtra("com.thenohotel.twilioapp.voice.GUEST_NAME");

        String url = String.format("%s?fromPhone=%s&toPhone=%s&fromToPhone=%s",API_END_POINT_CALL_LOG_DETAIL,fromPhoneNumber,toPhoneNumber,fromToPhoneNumber);

        Utils.JSONAsyncTask callLogDetailJsonTask = new Utils.JSONAsyncTask(new Utils.OnJSONTaskCompleted() {
            @Override
            public void onJSONTaskCompleted(JSONObject result) {
                try {
                    JSONArray cols = result.getJSONArray("DATA");
                    callLogDetailsObject = new ArrayList<>();
                    callLogDetailsObject.clear();
                    for (int i =0; i < cols.length(); i++) {
                        JSONArray callLogsThreads = cols.getJSONArray(i);
                        String fromPhone = callLogsThreads.get(0).toString();
                        String toPhone = callLogsThreads.get(1).toString();
                        String dateCreated = callLogsThreads.get(3).toString();
                        String fromToPhone = callLogsThreads.get(4).toString();
                        String hostFirstName = callLogsThreads.get(5).toString();
                        String hostLastName = callLogsThreads.get(6).toString();
                        String guestFirstName = callLogsThreads.get(7).toString();
                        String guestLastName = callLogsThreads.get(8).toString();
                        String hostName = String.format("%s %s",hostFirstName,hostLastName);
                        String hostPhoneNumber = fromPhone;
                        String guestPhoneNumber = toPhone;
                        String guestName = "";
                        if (!guestFirstName.equals("null")) {
                            guestName = String.format("%s %s",guestFirstName,guestLastName);
                        }
                        callLogDetailsObject.add(new CallLogDetailsObject(fromPhone,toPhone,dateCreated,fromToPhone,hostName,guestName,hostPhoneNumber,guestPhoneNumber));
                    }
                    recyclerViewCallDetail.setAdapter(new CallLogDetailAdapter(callLogDetailsObject));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        callLogDetailJsonTask.execute(url);
//        pbDetailMessage = findViewById(R.id.pbDetailMessage);
//        pbDetailMessage.getIndeterminateDrawable().setColorFilter(0xFFFF0000, android.graphics.PorterDuff.Mode.MULTIPLY);
        recyclerViewCallDetail = findViewById(R.id.recyclerViewCallLogDetail);
        txtHostName = findViewById(R.id.callDetailHost);
        txtGuestName = findViewById(R.id.callDetailGuest);
        txtHostName.setText(String.format("Host: %s",hostName));
        txtGuestName.setText(String.format("Guest: %s",guestName));
        recyclerViewCallDetail.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent parentIntent = NavUtils.getParentActivityIntent(this);
                if(parentIntent == null) {
                    finish();
                    return true;
                } else {
                    parentIntent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(parentIntent);
                    finish();
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }
}
