package twilioapp.thenohotel.com.twiliosmsvoice.SMS;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import twilioapp.thenohotel.com.twiliosmsvoice.R;
import twilioapp.thenohotel.com.twiliosmsvoice.Libraries.Utils;

import static twilioapp.thenohotel.com.twiliosmsvoice.Common.APIEndPoints.API_END_POINT_DELETE_SMS;

public class SMSListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<SMSListObject> data;

    public SMSListAdapter(ArrayList<SMSListObject> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == 1) {
            View view = inflater.inflate(R.layout.sms_list_item,parent,false);
            return new SMSListViewHolder(view);
        } else {
            View view = inflater.inflate(R.layout.anon_sms_list_item,parent,false);
            return new AnonSMSListViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final SMSListObject smsObj = data.get(position);
        switch (holder.getItemViewType()) {
            case 1:
                SMSListViewHolder listViewHolder = (SMSListViewHolder) holder;
                listViewHolder.txtSMSSender.setText(smsObj.getGuestName());
                listViewHolder.txtLastSMS.setText(smsObj.getLastSMS());
                listViewHolder.txtLastSMSTime.setText(smsObj.getLastSMSTimestamp());
                listViewHolder.txtLastSMS.setHint(String.valueOf(smsObj.getThreadID()));
                listViewHolder.txtBookingID.setText(String.valueOf(smsObj.getBookingID()));
                if (!smsObj.getIsRead()) {
                    listViewHolder.txtLastSMS.setTypeface(null, Typeface.BOLD);
                    listViewHolder.txtSMSSender.setTypeface(null,Typeface.BOLD);
                }
                Picasso.get().load(smsObj.getGuestPicURL()).into(listViewHolder.imgIcon);
                listViewHolder.imgDeleteIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getContext());
                        alertDialogBuilder.setTitle("Alert");
                        alertDialogBuilder.setMessage(String.format("Guest:%s\n\nAre you sure you want to delete this thread?",smsObj.getGuestName()));
                        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String url = String.format("%s?threadID=%s&bookingID=%s&hostPhone=%s&guestPhone=%s",API_END_POINT_DELETE_SMS,String.valueOf(smsObj.getThreadID()),String.valueOf(smsObj.getBookingID()),smsObj.getGuestPhoneNumber(),smsObj.getHostPhoneNumber());
                                RequestQueue MyRequestQueue = Volley.newRequestQueue(view.getContext());
                                StringRequest MyStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Toast.makeText(view.getContext(),"Message deleted Successfully!",Toast.LENGTH_LONG).show();
                                        data.remove(position);
                                        notifyItemRemoved(position);
                                        notifyItemRangeChanged(position,data.size());
                                    }
                                }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        //This code is executed if there is an error.
                                    }
                                });
                                MyRequestQueue.add(MyStringRequest);
                            }
                        });
                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        alertDialogBuilder.show();
                    }
                });
                break;
            case 2:
                AnonSMSListViewHolder anonListViewHolder = (AnonSMSListViewHolder) holder;
                anonListViewHolder.txtAnonLastSMS.setText(smsObj.getLastSMS());
                anonListViewHolder.txtAnonLastSMSDateTime.setText(smsObj.getLastSMSTimestamp());
                anonListViewHolder.txtAnonGuestHost.setText(String.format("Host: %s",smsObj.getHostName()));
                anonListViewHolder.txtAnonGuestNumber.setText(String.format("Guest: %s",Utils.formatPhone(smsObj.getGuestPhoneNumber())));
                anonListViewHolder.txtAnonGuestHost.setHint(smsObj.getHostPhoneNumber());
                anonListViewHolder.txtAnonGuestNumber.setHint(smsObj.getGuestPhoneNumber());
                anonListViewHolder.txtAnonBookingID.setText(String.valueOf(smsObj.getBookingID()));
                if (!smsObj.getIsRead()) {
                    anonListViewHolder.txtAnonLastSMS.setTypeface(null, Typeface.BOLD);
                    anonListViewHolder.txtAnonGuestNumber.setTypeface(null,Typeface.BOLD);
                }
                anonListViewHolder.anonImgDeleteIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getContext());
                        alertDialogBuilder.setTitle("Alert");
                        alertDialogBuilder.setMessage(String.format("Host:%s\n\nAre you sure you want to delete this thread?",smsObj.getHostName()));
                        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String url = String.format("%s?threadID=0&bookingID=%s&hostPhone=%s&guestPhone=%s",API_END_POINT_DELETE_SMS,String.valueOf(smsObj.getBookingID()),smsObj.getGuestPhoneNumber(),smsObj.getHostPhoneNumber());
                                RequestQueue MyRequestQueue = Volley.newRequestQueue(view.getContext());
                                StringRequest MyStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Toast.makeText(view.getContext(),"Message deleted Successfully!",Toast.LENGTH_LONG).show();
                                        data.remove(position);
                                        notifyItemRemoved(position);
                                        notifyItemRangeChanged(position,data.size());
                                    }
                                }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        //This code is executed if there is an error.
                                    }
                                });
                                MyRequestQueue.add(MyStringRequest);
                            }
                        });
                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        alertDialogBuilder.show();
                    }
                });
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position).getBookingID() > 0) {
            return 1;
        } else {
            return 2;
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class SMSListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgIcon;
        ImageView imgDeleteIcon;
        TextView txtLastSMS;
        TextView txtSMSSender;
        TextView txtLastSMSTime;
        TextView txtBookingID;

        public SMSListViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            imgIcon = itemView.findViewById(R.id.imgIcon);
            imgDeleteIcon = itemView.findViewById(R.id.imgDeleteIcon);
            txtLastSMS = itemView.findViewById(R.id.txtLastSMS);
            txtSMSSender = itemView.findViewById(R.id.txtSMSSender);
            txtLastSMSTime = itemView.findViewById(R.id.txtLastSMSDateTime);
            txtBookingID = itemView.findViewById(R.id.smsListBookingID);
        }

        @Override
        public void onClick(View view) {
            String threadID = txtLastSMS.getHint().toString();
            String bookingID = txtBookingID.getText().toString();
            Intent intent;
            intent = new Intent(view.getContext(), SMSDetailActivity.class);
            intent.putExtra("com.thenohotel.twilioapp.THREADID", threadID);
            intent.putExtra("com.thenohotel.twilioapp.BOOKINGID", bookingID);
            view.getContext().startActivity(intent);
        }
    }

    public class AnonSMSListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView anonImgDeleteIcon;
        TextView txtAnonGuestHost;
        TextView txtAnonLastSMSDateTime;
        TextView txtAnonLastSMS;
        TextView txtAnonGuestNumber;
        TextView txtAnonBookingID;

        public AnonSMSListViewHolder(View itemView) {
            super(itemView);
            txtAnonGuestHost = itemView.findViewById(R.id.txtAnonGuestHost);
            txtAnonLastSMSDateTime = itemView.findViewById(R.id.txtAnonLastSMSDateTime);
            txtAnonLastSMS = itemView.findViewById(R.id.txtAnonLastSMS);
            txtAnonGuestNumber = itemView.findViewById(R.id.txtAnonGuestNumber);
            anonImgDeleteIcon = itemView.findViewById(R.id.anonImgDeleteIcon);
            txtAnonBookingID = itemView.findViewById(R.id.anonSMSListBookingID);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            String hostNumber = txtAnonGuestHost.getHint().toString();
            String guestNumber = txtAnonGuestNumber.getHint().toString();
            String bookingID = txtAnonBookingID.getText().toString();
            Intent intent;
            intent = new Intent(view.getContext(), AnonSMSDetailActivity.class);
            intent.putExtra("com.thenohotel.twilioapp.hostNumber", hostNumber);
            intent.putExtra("com.thenohotel.twilioapp.guestNumber", guestNumber);
            intent.putExtra("com.thenohotel.twilioapp.BOOKINGID", bookingID);
            view.getContext().startActivity(intent);
        }
    }
}