package twilioapp.thenohotel.com.twiliosmsvoice.SMS;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import twilioapp.thenohotel.com.twiliosmsvoice.R;

public class AnonSMSDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<AnonSMSSingleItemObject> threadMessages;

    public AnonSMSDetailAdapter(ArrayList<AnonSMSSingleItemObject> threadMessages) {
        this.threadMessages = threadMessages;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        if (viewType == 1) {
            View view = layoutInflater.inflate(R.layout.anon_sms_detail_receiver_item,parent,false);
            return new SMSDetailReceiverViewHolder(view);
        } else {
            View view = layoutInflater.inflate(R.layout.anon_sms_detail_sender_item,parent,false);
            return new SMSDetailSenderViewHolder(view);
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (threadMessages.get(position).getMsgFrom().equals("Host")) {
            return 1;
        } else {
            return 2;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        AnonSMSSingleItemObject SMSItem = threadMessages.get(position);
        switch (holder.getItemViewType()) {
            case 1:
                SMSDetailReceiverViewHolder receiverViewHolder = (SMSDetailReceiverViewHolder) holder;
                receiverViewHolder.receiverSMS.setText(SMSItem.getSMSMessage().toString());
                receiverViewHolder.receiverSMSTime.setText(SMSItem.getMsgTimestamp().toString());
                break;
            case 2:
                SMSDetailSenderViewHolder senderViewHolder = (SMSDetailSenderViewHolder) holder;
                senderViewHolder.senderSMS.setText(SMSItem.getSMSMessage().toString());
                senderViewHolder.senderSMSTime.setText(SMSItem.getMsgTimestamp().toString());
                break;
        }

    }

    @Override
    public int getItemCount() {
        return threadMessages.size();
    }

    public class SMSDetailSenderViewHolder extends RecyclerView.ViewHolder {
        TextView senderSMS;
        TextView senderSMSTime;
        public SMSDetailSenderViewHolder(View itemView) {
            super(itemView);
            senderSMS = itemView.findViewById(R.id.txtAnonSMSDetailSenderMessage);
            senderSMSTime = itemView.findViewById(R.id.txtAnonSMSDetailSenderTime);
        }
    }
    public class SMSDetailReceiverViewHolder extends RecyclerView.ViewHolder {
        TextView receiverSMS;
        TextView receiverSMSTime;
        public SMSDetailReceiverViewHolder(View itemView) {
            super(itemView);
            receiverSMS = itemView.findViewById(R.id.txtAnonSMSDetailReceiverMessage);
            receiverSMSTime = itemView.findViewById(R.id.txtAnonSMSDetailReceiverTime);
        }
    }
}
