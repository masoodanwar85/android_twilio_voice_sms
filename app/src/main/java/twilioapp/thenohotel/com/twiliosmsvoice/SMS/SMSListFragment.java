package twilioapp.thenohotel.com.twiliosmsvoice.SMS;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import twilioapp.thenohotel.com.twiliosmsvoice.Common.GuestPhone;
import twilioapp.thenohotel.com.twiliosmsvoice.Common.GuestSpinnerAdapter;
import twilioapp.thenohotel.com.twiliosmsvoice.Common.HostPhone;
import twilioapp.thenohotel.com.twiliosmsvoice.Common.HostSpinnerAdapter;
import twilioapp.thenohotel.com.twiliosmsvoice.Libraries.Utils;
import twilioapp.thenohotel.com.twiliosmsvoice.R;

import static twilioapp.thenohotel.com.twiliosmsvoice.Common.APIEndPoints.API_END_POINT_SEND_MESSAGE;
import static twilioapp.thenohotel.com.twiliosmsvoice.Common.APIEndPoints.API_END_POINT_SMS_THREADS;

public class SMSListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private static String TAG = "SMSListFragment";
    private static ArrayList<GuestPhone> smsGuestsPhones;
    private static ArrayList<HostPhone> smsHostPhones;
    RecyclerView smsList;
    public AlertDialog alertDialog;
    private ArrayList<SMSListObject> smsListObjects;
    ProgressBar progressBar;
    static ProgressBar dialogProgressBar;
    public static EditText msgBox;
    static TextView txtGuestSMSPhone;
    static TextView txtHostSMSPhone;
    static TextView txtDialPhone;
    SwipeRefreshLayout pullToRefresh;
    private static GuestPhone lastSelectedGuestObj = new GuestPhone("","","","","",0);
    private FloatingActionButton smsActionFab;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

        }
    }

    private View.OnClickListener smsActionFabClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = createSMSDialog(getContext());
                alertDialog.show();
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = API_END_POINT_SEND_MESSAGE;

                        final String toNumberValue = txtGuestSMSPhone.getText().toString();
                        final String toDialNumberValue = txtDialPhone.getText().toString();
                        final String fromNumberValue = txtHostSMSPhone.getText().toString();
                        final String messageValue = msgBox.getText().toString();

                        if (fromNumberValue.equals("")) {
                            Toast.makeText(getContext(),"Please Select Host",Toast.LENGTH_LONG).show();
                        } else if (toNumberValue.equals("") && toDialNumberValue.equals("")) {
                            Toast.makeText(getContext(),"Phone Number is required",Toast.LENGTH_LONG).show();
                        } else if (messageValue.equals("")) {
                            Toast.makeText(getContext(),"Message is required",Toast.LENGTH_LONG).show();
                        } else {
                            dialogProgressBar.setVisibility(View.VISIBLE);
                            // Disable Send SMS button
                            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                            RequestQueue MyRequestQueue = Volley.newRequestQueue(getContext());
                            StringRequest MyStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    //This code is executed if the server responds, whether or not the response contains data.
                                    //The String 'response' contains the server's response.
                                    Toast.makeText(getContext(),"Message Sent Successfully!",Toast.LENGTH_LONG).show();
                                    msgBox.setText("");
                                    dialogProgressBar.setVisibility(View.GONE);
                                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                                    alertDialog.dismiss();
                                    getSMSThreads();
                                }
                            }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    //This code is executed if there is an error.
                                }
                            }) {
                                protected Map<String, String> getParams() {
                                    Map<String, String> MyData = new HashMap<String, String>();
                                    MyData.put("toNumber",toDialNumberValue.equals("") ? toNumberValue : toDialNumberValue);
                                    MyData.put("fromNumber",fromNumberValue);
                                    MyData.put("message",messageValue);
                                    return MyData;
                                }
                            };
                            MyRequestQueue.add(MyStringRequest);
                        }
                    }
                });
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public static AlertDialog createSMSDialog(final Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setIcon(R.drawable.ic_textsms);
        alertDialogBuilder.setTitle("SMS");
        alertDialogBuilder.setPositiveButton("Send SMS", null);
        alertDialogBuilder.setNegativeButton("Cancel", null);
        alertDialogBuilder.setCancelable(false);

        LayoutInflater li = LayoutInflater.from(context);
        View dialogView = li.inflate(R.layout.dialog_sms, null);
        dialogProgressBar = dialogView.findViewById(R.id.SMSDialogProgressBar);
        dialogProgressBar.getIndeterminateDrawable().setColorFilter(0xFFFF0000, android.graphics.PorterDuff.Mode.MULTIPLY);
        dialogProgressBar.bringToFront();

        txtGuestSMSPhone = dialogView.findViewById(R.id.guestSMSPhone);
        txtHostSMSPhone = dialogView.findViewById(R.id.hostSMSPhone);
        txtDialPhone = dialogView.findViewById(R.id.txtNumber);

        //final TextView smsHostInfo = dialogView.findViewById(R.id.SMSHostInfo);
        final Spinner smsGuestSpinner = dialogView.findViewById(R.id.smsGuestSpinner);
        final Spinner smsHostSpinner = dialogView.findViewById(R.id.smsHostSpinner);
        final Button btnClear = dialogView.findViewById(R.id.btnClear);
        msgBox = dialogView.findViewById(R.id.txtMessage);
        final GuestSpinnerAdapter smsGuestAdapter = new GuestSpinnerAdapter(context,R.layout.support_simple_spinner_dropdown_item,smsGuestsPhones);
        smsGuestSpinner.setAdapter(smsGuestAdapter);
        final HostSpinnerAdapter smsHostAdapter = new HostSpinnerAdapter(context,R.layout.support_simple_spinner_dropdown_item,smsHostPhones);
        smsHostSpinner.setAdapter(smsHostAdapter);

        smsHostSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                HostPhone selectedHostPhone = (HostPhone) adapterView.getItemAtPosition(position);
                String hostPhoneNumber = selectedHostPhone.getHostTwilioPhone();
                txtHostSMSPhone.setText(hostPhoneNumber);
                HashMap<String,String> smsData = smsGuestAdapter.updateList(smsGuestsPhones,selectedHostPhone.getAccountID(),txtGuestSMSPhone.getText().toString());

                int smsIdx = 0;
                if (selectedHostPhone.getAccountID() == 0) {
                    txtGuestSMSPhone.setText("");
                    txtHostSMSPhone.setText("");
                } else {
                    smsIdx = Integer.parseInt(smsData.get("selectedIndex"));
                    txtGuestSMSPhone.setText(smsData.get("guestPhone"));
                }
                smsGuestSpinner.setSelection(smsIdx);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        smsGuestSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                GuestPhone selectedGuestPhone = (GuestPhone) parent.getItemAtPosition(position);
                String guestPhoneNumber = selectedGuestPhone.getValue();
                txtGuestSMSPhone.setText(guestPhoneNumber);
                int idx = smsHostPhones.indexOf(new HostPhone(selectedGuestPhone.getHostName(),selectedGuestPhone.getHostTwilioPhone(),selectedGuestPhone.getAccountID()));
                lastSelectedGuestObj = selectedGuestPhone;
                smsHostSpinner.setSelection(idx);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtDialPhone.setText("");
            }
        });

        alertDialogBuilder.setView(dialogView);
        return alertDialogBuilder.create();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View SMSListFragmentView = inflater.inflate(R.layout.sms_list_fragment,container,false);
        pullToRefresh = SMSListFragmentView.findViewById(R.id.swipe_container);
        pullToRefresh.setOnRefreshListener(this);
        return SMSListFragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getSMSThreads();
    }

    public void setSmsGuestsPhones(ArrayList<GuestPhone> smsGuestsPhones) {
        this.smsGuestsPhones = smsGuestsPhones;
    }

    public void setSmsHostsPhones(ArrayList<HostPhone> smsHostsPhones) {
        this.smsHostPhones = smsHostsPhones;
    }

    private void getSMSThreads() {
        Utils.JSONAsyncTask SMSJson = new Utils.JSONAsyncTask(new Utils.OnJSONTaskCompleted() {
            @Override
            public void onJSONTaskCompleted(JSONObject result) {
                try {
                    JSONArray cols = result.getJSONArray("DATA");
                    smsListObjects = new ArrayList<>();
                    smsListObjects.clear();
                    for (int i =0; i < cols.length(); i++) {
                        JSONArray twilioSMSThread = cols.getJSONArray(i);
                        int bookingID = (int) twilioSMSThread.get(0);
                        int threadID = (int) twilioSMSThread.get(1);
                        String fromPhone = twilioSMSThread.get(2).toString();
                        String toPhone = twilioSMSThread.get(3).toString();
                        String message = twilioSMSThread.get(4).toString();
                        String dateCreated = twilioSMSThread.get(6).toString();
                        String guestFirstName = twilioSMSThread.get(7).toString();
                        String guestLastName = twilioSMSThread.get(8).toString();
                        String guestPicURL = twilioSMSThread.get(9).toString();
                        int isReadValue = Integer.parseInt(twilioSMSThread.get(10).toString());
                        String twilioPhoneNumber = twilioSMSThread.get(11).toString();
                        String msgFrom = twilioSMSThread.get(12).toString();
                        String hostGuestNumber = twilioSMSThread.get(13).toString();
                        String hostFirstName = twilioSMSThread.get(14).toString();
                        String hostLastName = twilioSMSThread.get(15).toString();
                        boolean isRead = false;
                        if (isReadValue == 1) {
                            isRead = true;
                        }

                        String guestPhone = fromPhone;
                        String hostPhone = toPhone;
                        if (msgFrom.equals("Host")) {
                            guestPhone = toPhone;
                            hostPhone = fromPhone;
                        }

                        String guestName = guestFirstName + " " + guestLastName;
                        String hostName = hostFirstName;
                        smsListObjects.add(new SMSListObject(isRead,bookingID,threadID,message,dateCreated,guestPicURL,fromPhone,toPhone,guestName,twilioPhoneNumber,msgFrom,hostGuestNumber,hostName,hostPhone,guestPhone));
                    }

                    smsList.setAdapter(new SMSListAdapter(smsListObjects));
                    progressBar.setVisibility(getView().INVISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        SMSJson.execute(API_END_POINT_SMS_THREADS);

        progressBar = getActivity().findViewById(R.id.progressBar);
        progressBar.setVisibility(getView().VISIBLE);
        progressBar.bringToFront();
        smsList = getActivity().findViewById(R.id.smsList);
        smsActionFab = getActivity().findViewById(R.id.sms_action_fab);
        smsActionFab.setOnClickListener(smsActionFabClickListener());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        smsList.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(smsList.getContext(),DividerItemDecoration.VERTICAL);
        smsList.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void onRefresh() {
        getSMSThreads();
        pullToRefresh.setRefreshing(false);
    }
}