package twilioapp.thenohotel.com.twiliosmsvoice.Voice;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.twilio.voice.Voice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import twilioapp.thenohotel.com.twiliosmsvoice.Common.GuestPhone;
import twilioapp.thenohotel.com.twiliosmsvoice.Common.GuestSpinnerAdapter;
import twilioapp.thenohotel.com.twiliosmsvoice.Common.HostPhone;
import twilioapp.thenohotel.com.twiliosmsvoice.Common.HostSpinnerAdapter;
import twilioapp.thenohotel.com.twiliosmsvoice.Libraries.Utils;
import twilioapp.thenohotel.com.twiliosmsvoice.R;

import static twilioapp.thenohotel.com.twiliosmsvoice.Common.APIEndPoints.API_END_POINT_ACCESS_TOKEN;
import static twilioapp.thenohotel.com.twiliosmsvoice.Common.APIEndPoints.API_END_POINT_CALL_LOGS;

public class CallLogsFragment extends Fragment {
    private static String TAG = "CallLogsFragment";
    private ArrayList<CallLogsObject> callLogs;
    private static ArrayList<CallLogsObject> callLogsArrayList = new ArrayList<>();
    RecyclerView callLogThreads;
    private static String identity = "";
    private static final int MIC_PERMISSION_REQUEST_CODE = 1;
    static String accessToken;
    private FloatingActionButton callActionFab;
    private AlertDialog alertDialog;
    static ProgressBar voiceProgressBar;
    private static ArrayList<GuestPhone> voiceGuestsPhones;
    private static ArrayList<HostPhone> voiceHostPhones;
    private static TextView txtErrorMessage;
    static TextView txtGuestPhoneNumber;
    static TextView txtHostPhoneNumber;
    static TextView txtGuestName;
    static TextView voiceGuestNumber;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View voiceFragmentView = inflater.inflate(R.layout.activity_call_logs,container,false);
//        pullToRefresh = SMSListFragmentView.findViewById(R.id.swipe_container);
//        pullToRefresh.setOnRefreshListener(this);
        return voiceFragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getCallLogThreads();
        callActionFab = view.findViewById(R.id.call_action_fab);
        callActionFab.setOnClickListener(callActionFabClickListener());
    }

    @Override
    public void onResume() {
        super.onResume();
        getCallLogThreads();
    }

    private void getCallLogThreads() {
        Utils.JSONAsyncTask callLogsJSON = new Utils.JSONAsyncTask(new Utils.OnJSONTaskCompleted() {
            @Override
            public void onJSONTaskCompleted(JSONObject result) {
                try {
                    JSONArray cols = result.getJSONArray("DATA");
                    callLogsArrayList.clear();
                    for (int i =0; i < cols.length(); i++) {
                        JSONArray callLogsThreads = cols.getJSONArray(i);
                        int bookingID = (int) callLogsThreads.get(1);
                        String callSID = callLogsThreads.get(2).toString();
                        String direction = callLogsThreads.get(10).toString();
                        String fromPhone = callLogsThreads.get(3).toString();
                        String toPhone = callLogsThreads.get(4).toString();
                        if (direction.equals("inbound")) {
                            fromPhone = callLogsThreads.get(4).toString();
                            toPhone = callLogsThreads.get(3).toString();
                        }

                        String dateCreated = callLogsThreads.get(6).toString();
                        String fromToPhone = callLogsThreads.get(7).toString();
                        String callStatus = callLogsThreads.get(8).toString();
                        int duration = (int) callLogsThreads.get(9);
                        String hostFirstName = callLogsThreads.get(11).toString();
                        String hostLastName = callLogsThreads.get(12).toString();
                        String guestFirstName = callLogsThreads.get(13).toString();
                        String guestLastName = callLogsThreads.get(14).toString();
                        String hostName = String.format("%s %s",hostFirstName,hostLastName);
                        String hostPhoneNumber = fromPhone;
                        String guestPhoneNumber = toPhone;
                        String guestName = "";
                        if (!guestFirstName.equals("null")) {
                            guestName = String.format("%s %s",guestFirstName,guestLastName);
                        }
                        callLogsArrayList.add(new CallLogsObject(bookingID, callSID, callStatus, duration, direction, fromPhone, toPhone, dateCreated, fromToPhone, hostName, guestName, hostPhoneNumber, guestPhoneNumber));
                    }

                    callLogThreads.setAdapter(new CallLogsAdapter(callLogsArrayList));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        callLogsJSON.execute(API_END_POINT_CALL_LOGS);
//        progressBar = getActivity().findViewById(R.id.progressBar);
//        progressBar.setVisibility(getView().VISIBLE);
//        progressBar.bringToFront();
        callLogThreads = getActivity().findViewById(R.id.callsLog);
//        smsActionFab = getActivity().findViewById(R.id.sms_action_fab);
//        smsActionFab.setOnClickListener(smsActionFabClickListener());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        callLogThreads.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(callLogThreads.getContext(),DividerItemDecoration.VERTICAL);
        callLogThreads.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (!checkPermissionForMicrophone()) {
                requestPermissionForMicrophone();
            } else {
                //retrieveAccessToken();
            }
        }
    }

    private View.OnClickListener callActionFabClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = createCallDialog(getContext());
                alertDialog.show();
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String callToPhoneNumber = voiceGuestNumber.getText().toString();
                        String callToGuestName = "";
                        if (callToPhoneNumber.equals("")) {
                            callToPhoneNumber = txtGuestPhoneNumber.getText().toString();
                            callToGuestName = txtGuestName.getText().toString();
                        }

                        if (txtHostPhoneNumber.getText().toString().equals("")) {
                            txtErrorMessage.setText("Please Select Host");
                            txtErrorMessage.setVisibility(View.VISIBLE);
                        } else if (callToPhoneNumber.equals("")) {
                            txtErrorMessage.setText("Please Select Guest");
                            txtErrorMessage.setVisibility(View.VISIBLE);
                        } else {
                            txtErrorMessage.setVisibility(View.INVISIBLE);
                            alertDialog.dismiss();
                            Intent intent;
                            intent = new Intent(view.getContext(), ActiveCallActivity.class);
                            intent.putExtra("com.thenohotel.twilioapp.voice.CALL_TO", callToPhoneNumber);
                            intent.putExtra("com.thenohotel.twilioapp.voice.CALL_TO_GUEST_NAME", callToGuestName);
                            intent.putExtra("com.thenohotel.twilioapp.voice.ACCESS_TOKEN", accessToken);
                            intent.putExtra("com.thenohotel.twilioapp.voice.CALL_FROM", txtHostPhoneNumber.getText().toString());
                            view.getContext().startActivity(intent);
                        }
                    }
                });

                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
            }
        };
    }

    private boolean checkPermissionForMicrophone() {
        int resultMic = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO);
        return resultMic == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissionForMicrophone() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.RECORD_AUDIO)) {
            Toast.makeText(getActivity(),"Microphone permissions needed. Please allow in your application settings.",Toast.LENGTH_LONG);
        } else {
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    MIC_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        /*
         * Check if microphone permissions is granted
         */
        if (requestCode == MIC_PERMISSION_REQUEST_CODE && permissions.length > 0) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(),"Microphone permissions needed. Please allow in your application settings.",Toast.LENGTH_LONG);
            } else {
                //getAccessToken(getContext());
            }
        }
    }

    public static AlertDialog createCallDialog(final Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setIcon(R.drawable.ic_call_black_24dp);
        alertDialogBuilder.setTitle("Call");
        alertDialogBuilder.setPositiveButton("Call", null);
        alertDialogBuilder.setNegativeButton("Cancel", null);
        alertDialogBuilder.setCancelable(false);

        LayoutInflater li = LayoutInflater.from(context);
        View dialogView = li.inflate(R.layout.dialog_call, null);

        txtGuestPhoneNumber = dialogView.findViewById(R.id.guestVoicePhone);
        txtHostPhoneNumber = dialogView.findViewById(R.id.hostVoicePhone);
        txtGuestName = dialogView.findViewById(R.id.guestVoiceName);

        final Spinner voiceGuestSpinner = dialogView.findViewById(R.id.voiceGuestSpinner);
        final Spinner voiceHostSpinner = dialogView.findViewById(R.id.voiceHostSpinner);
        final Button btnClearVoice = dialogView.findViewById(R.id.btnClearVoice);
        voiceGuestNumber = dialogView.findViewById(R.id.txtVoiceNumber);
        txtErrorMessage = dialogView.findViewById(R.id.txtErrorMsg);

        voiceProgressBar = dialogView.findViewById(R.id.voiceProgressBar);
        voiceProgressBar.getIndeterminateDrawable().setColorFilter(0xFFFF0000, android.graphics.PorterDuff.Mode.MULTIPLY);
        voiceProgressBar.bringToFront();

        final GuestSpinnerAdapter voiceGuestAdapter = new GuestSpinnerAdapter(context,R.layout.support_simple_spinner_dropdown_item,voiceGuestsPhones);
        voiceGuestSpinner.setAdapter(voiceGuestAdapter);

        final HostSpinnerAdapter voiceHostAdapter = new HostSpinnerAdapter(context,R.layout.support_simple_spinner_dropdown_item,voiceHostPhones);
        voiceHostSpinner.setAdapter(voiceHostAdapter);

        voiceHostSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                txtErrorMessage.setVisibility(View.INVISIBLE);
                HostPhone selectedHostPhone = (HostPhone) adapterView.getItemAtPosition(position);
                txtHostPhoneNumber.setText(selectedHostPhone.getHostTwilioPhone());
                HashMap<String,String> voiceData = voiceGuestAdapter.updateList(voiceGuestsPhones,selectedHostPhone.getAccountID(),txtGuestPhoneNumber.getText().toString());
                identity = selectedHostPhone.getHostTwilioPhone();
                getAccessToken(context);
                int voiceIdx = 0;
                if (selectedHostPhone.getAccountID() == 0) {
                    txtGuestPhoneNumber.setText("");
                    txtGuestName.setText("");
                } else {
                    voiceIdx = Integer.parseInt(voiceData.get("selectedIndex"));
                    txtGuestName.setText(voiceData.get("guestName"));
                    txtGuestPhoneNumber.setText(voiceData.get("guestPhone"));
                }
                voiceGuestSpinner.setSelection(voiceIdx);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        voiceGuestSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                GuestPhone selectedGuestPhone = (GuestPhone) parent.getItemAtPosition(position);
                txtGuestPhoneNumber.setText(selectedGuestPhone.getValue());
                txtGuestName.setText(selectedGuestPhone.getGuestName());
                int idx = voiceHostPhones.indexOf(new HostPhone(selectedGuestPhone.getHostName(),selectedGuestPhone.getHostTwilioPhone(),selectedGuestPhone.getAccountID()));
                voiceHostSpinner.setSelection(idx);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnClearVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                voiceGuestNumber.setText("");
            }
        });

        //contact.setHint(R.string.callee);
        alertDialogBuilder.setView(dialogView);

        return alertDialogBuilder.create();

    }

    /*
     * Get an access token from your Twilio access token server
     */

    private static void getAccessToken(Context context) {
        voiceProgressBar.setVisibility(View.VISIBLE);
        String url = API_END_POINT_ACCESS_TOKEN + "?identity="+identity.trim();
        RequestQueue MyRequestQueue = Volley.newRequestQueue(context);
        StringRequest MyStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
                accessToken = response;
                voiceProgressBar.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
            }
        });
        MyRequestQueue.add(MyStringRequest);
    }

    public void setVoiceGuestsPhones(ArrayList<GuestPhone> voiceGuestsPhones) {
        this.voiceGuestsPhones = voiceGuestsPhones;
    }

    public void setVoiceHostPhones(ArrayList<HostPhone> voiceHostPhones) {
        this.voiceHostPhones = voiceHostPhones;
    }
}