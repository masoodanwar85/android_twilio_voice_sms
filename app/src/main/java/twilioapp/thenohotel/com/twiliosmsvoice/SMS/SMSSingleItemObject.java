package twilioapp.thenohotel.com.twiliosmsvoice.SMS;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SMSSingleItemObject {
    private String msgFrom;
    private String message;
    private String msgTimestamp;

    public SMSSingleItemObject(String msgFrom, String message, String msgTimestamp) {
        this.msgFrom = msgFrom;
        this.message = message;
        this.msgTimestamp = msgTimestamp;
    }

    public String getMsgFrom() {
        return msgFrom;
    }

    public void setMsgFrom(String msgFrom) {
        this.msgFrom = msgFrom;
    }

    public String getSMSMessage() {
        return message;
    }

    public void setSMSMessage(String message) {
        this.message = message;
    }

    public String getMsgTimestamp() {
        try {
            Date SMSDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this.msgTimestamp);
            //SimpleDateFormat dtFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
            SimpleDateFormat dtFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
            return dtFormat.format(SMSDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setMsgTimestamp(String msgTimestamp) {
        this.msgTimestamp = msgTimestamp;
    }
}
