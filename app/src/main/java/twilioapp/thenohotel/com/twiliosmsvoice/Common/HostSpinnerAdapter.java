package twilioapp.thenohotel.com.twiliosmsvoice.Common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class HostSpinnerAdapter extends ArrayAdapter<HostPhone> {
    private Context context;
    private ArrayList<HostPhone> myHostPhones;

    public HostSpinnerAdapter(@NonNull Context context, int textViewResourceId, ArrayList<HostPhone> myHostPhones) {
        super(context, textViewResourceId, myHostPhones);
        this.context = context;
        this.myHostPhones = myHostPhones;
    }


    public int getCount() {
        return myHostPhones.size();
    }

    public HostPhone getItem(int position) {
        return myHostPhones.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextSize(18);
        label.setText(myHostPhones.get(position).getHostName());
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextSize(18);
        label.setPadding(15,15,15,18);
        label.setText(myHostPhones.get(position).getHostName());
        return label;
    }
}
