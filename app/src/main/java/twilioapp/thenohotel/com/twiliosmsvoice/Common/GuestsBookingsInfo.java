package twilioapp.thenohotel.com.twiliosmsvoice.Common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class GuestsBookingsInfo {
    private int bookingID;
    private String guestName;
    private String guestPhone;
    private String property;
    private String hostName;
    private String hostPhone;
    private String checkinDate;
    private String checkoutDate;
    private String guestPicURL;

    public GuestsBookingsInfo(int bookingID, String guestName, String guestPhone, String property, String hostName, String hostPhone,String checkinDate,String checkoutDate,String guestPicURL) {
        this.bookingID = bookingID;
        this.guestName = guestName;
        this.guestPhone = guestPhone;
        this.property = property;
        this.hostName = hostName;
        this.hostPhone = hostPhone;
        this.checkinDate = checkinDate;
        this.checkoutDate = checkoutDate;
        this.guestPicURL = guestPicURL;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getGuestPhone() {
        return guestPhone;
    }

    public void setGuestPhone(String guestPhone) {
        this.guestPhone = guestPhone;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getHostPhone() {
        return hostPhone;
    }

    public void setHostPhone(String hostPhone) {
        this.hostPhone = hostPhone;
    }

    public static GuestsBookingsInfo findByHostPhone(ArrayList<GuestsBookingsInfo> guestsBookingsInfos,String guestPhoneNumber) {
        Iterator<GuestsBookingsInfo> iterator = guestsBookingsInfos.iterator();
        while (iterator.hasNext()) {
            GuestsBookingsInfo guestsBookingsInfo = iterator.next();
            if (guestsBookingsInfo.getGuestPhone().equals(guestPhoneNumber)) {
                return guestsBookingsInfo;
            }
        }
        return null;
    }

    public String getCheckinDate() {
        try {
            Date SMSDateTime = new SimpleDateFormat("yyyy-MM-dd").parse(this.checkinDate);
            SimpleDateFormat dtFormat = new SimpleDateFormat("MMM dd, yyyy");
            return dtFormat.format(SMSDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setCheckinDate(String checkinDate) {
        this.checkinDate = checkinDate;
    }

    public String getCheckoutDate() {
        try {
            Date SMSDateTime = new SimpleDateFormat("yyyy-MM-dd").parse(this.checkoutDate);
            SimpleDateFormat dtFormat = new SimpleDateFormat("MMM dd, yyyy");
            return dtFormat.format(SMSDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setCheckoutDate(String checkoutDate) {
        this.checkoutDate = checkoutDate;
    }

    public String getGuestPicURL() {
        return guestPicURL;
    }

    public void setGuestPicURL(String guestPicURL) {
        this.guestPicURL = guestPicURL;
    }
}
