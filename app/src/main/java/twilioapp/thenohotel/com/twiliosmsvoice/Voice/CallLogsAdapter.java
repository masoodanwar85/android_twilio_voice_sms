package twilioapp.thenohotel.com.twiliosmsvoice.Voice;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import twilioapp.thenohotel.com.twiliosmsvoice.R;
import twilioapp.thenohotel.com.twiliosmsvoice.SMS.AnonSMSDetailActivity;

public class CallLogsAdapter extends RecyclerView.Adapter<CallLogsAdapter.CallLogsViewHolder> {
    private ArrayList<CallLogsObject> callLogsData;

    public CallLogsAdapter(ArrayList<CallLogsObject> callLogsData) {
        this.callLogsData = callLogsData;
    }

    @NonNull
    @Override
    public CallLogsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.call_log_item,parent,false);
        return new CallLogsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CallLogsViewHolder holder, int position) {
        final CallLogsObject callLogsObj = callLogsData.get(position);
        CallLogsViewHolder callListViewHolder = holder;
        callListViewHolder.txtHostName.setText(callLogsObj.getHostName());
        callListViewHolder.txtTimeStamp.setText(callLogsObj.getDateCreated());
        callListViewHolder.txtFromPhone.setText(callLogsObj.getFromPhone());
        callListViewHolder.txtFromPhone.setHint(callLogsObj.getHostName());
        callListViewHolder.txtToPhone.setText(callLogsObj.getToPhone());

        if (callLogsObj.getGuestName().equals("")) {
            callListViewHolder.txtToPhone.setHint(callLogsObj.getGuestPhoneNumber());
            callListViewHolder.txtGuestName.setText(callLogsObj.getGuestPhoneNumber());
        } else {
            callListViewHolder.txtToPhone.setHint(callLogsObj.getGuestName());
            callListViewHolder.txtGuestName.setText(callLogsObj.getGuestName());
        }

        String callStatus = callLogsObj.getCallStatus();
        if (callLogsObj.getDirection().equals("outbound")) {
            callListViewHolder.imgCallDirection.setImageResource(R.drawable.ic_phone_outgoing_black_24dp);
        } else {
            callListViewHolder.imgCallDirection.setImageResource(R.drawable.ic_phone_incoming_black_24dp);
        }

        if (callStatus.equals("completed")) {
            callListViewHolder.txtCallStatusDuration.setText(String.valueOf(callLogsObj.getDuration()) + " sec");
        } else if (callStatus.equals("ringing")) {
            callListViewHolder.txtCallStatusDuration.setText("");
        } else {
            callListViewHolder.txtCallStatusDuration.setText(callStatus);
        }

        callListViewHolder.txtFromToPhone.setText(callLogsObj.getFromToPhone());

        callListViewHolder.imgCallIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(view.getContext(), ActiveCallActivity.class);
                intent.putExtra("com.thenohotel.twilioapp.voice.CALL_TO", callLogsObj.getGuestPhoneNumber());
                intent.putExtra("com.thenohotel.twilioapp.voice.CALL_TO_GUEST_NAME", callLogsObj.getGuestName());
                //intent.putExtra("com.thenohotel.twilioapp.voice.ACCESS_TOKEN", accessToken);
                intent.putExtra("com.thenohotel.twilioapp.voice.CALL_FROM", callLogsObj.getHostPhoneNumber());
                view.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return callLogsData.size();
    }

    public class CallLogsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgCallDirection;
        ImageView imgCallIcon;
        TextView txtHostName;
        TextView txtTimeStamp;
        TextView txtFromPhone;
        TextView txtToPhone;
        TextView txtFromToPhone;
        TextView txtGuestName;
        TextView callBookingID;
        TextView txtCallStatusDuration;

        public CallLogsViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            imgCallIcon = itemView.findViewById(R.id.callGuest);
            imgCallDirection = itemView.findViewById(R.id.callDirection);
            txtHostName = itemView.findViewById(R.id.callLogHostName);
            txtTimeStamp = itemView.findViewById(R.id.callLogDateTime);
            txtFromPhone = itemView.findViewById(R.id.fromPhone);
            txtToPhone = itemView.findViewById(R.id.toPhone);
            txtFromToPhone = itemView.findViewById(R.id.fromToPhone);
            txtGuestName = itemView.findViewById(R.id.callLogGuestName);
            callBookingID = itemView.findViewById(R.id.callBookingID);
            txtCallStatusDuration = itemView.findViewById(R.id.callStatusDuration);
        }

        @Override
        public void onClick(View view) {
//            Intent intent;
//            intent = new Intent(view.getContext(), CallLogDetailActivity.class);
//            intent.putExtra("com.thenohotel.twilioapp.voice.FROM_PHONE", txtFromPhone.getText().toString());
//            intent.putExtra("com.thenohotel.twilioapp.voice.TO_PHONE", txtToPhone.getText().toString());
//            intent.putExtra("com.thenohotel.twilioapp.voice.FROM_TO_PHONE", txtFromToPhone.getText().toString());
//            intent.putExtra("com.thenohotel.twilioapp.voice.HOST_NAME", txtFromPhone.getHint());
//            intent.putExtra("com.thenohotel.twilioapp.voice.GUEST_NAME", txtToPhone.getHint());
//            view.getContext().startActivity(intent);
        }
    }
}