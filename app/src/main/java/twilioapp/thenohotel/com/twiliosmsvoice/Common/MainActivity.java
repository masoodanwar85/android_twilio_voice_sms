package twilioapp.thenohotel.com.twiliosmsvoice.Common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.twilio.voice.RegistrationException;
import com.twilio.voice.RegistrationListener;
import com.twilio.voice.Voice;

import io.fabric.sdk.android.Fabric;
import twilioapp.thenohotel.com.twiliosmsvoice.Libraries.Utils;
import twilioapp.thenohotel.com.twiliosmsvoice.R;
import twilioapp.thenohotel.com.twiliosmsvoice.SMS.SMSListFragment;
import twilioapp.thenohotel.com.twiliosmsvoice.Voice.ActiveCallActivity;
import twilioapp.thenohotel.com.twiliosmsvoice.Voice.CallLogsFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static twilioapp.thenohotel.com.twiliosmsvoice.Common.APIEndPoints.API_END_POINT_ACCESS_TOKEN;
import static twilioapp.thenohotel.com.twiliosmsvoice.Common.APIEndPoints.API_END_POINT_BOOKINGS_GUEST_HOST_DD;
import static twilioapp.thenohotel.com.twiliosmsvoice.Common.APIEndPoints.API_END_POINT_BOOKINGS_INFO;

public class MainActivity extends AppCompatActivity  {

    private static final String TAG = "MyMainActivity";
    static MainActivity instance;
    private SampleAdapter mAdapter;
    private ViewPager viewPager;
    private static ArrayList<GuestPhone> guestPhones = new ArrayList<>();
    private static ArrayList<HostPhone> hostPhones = new ArrayList<>();
    public static ArrayList<GuestsBookingsInfo> guestsBookingsInfos = new ArrayList<>();

    private boolean isReceiverRegistered = false;

    public static final String INCOMING_CALL_INVITE = "INCOMING_CALL_INVITE";
    public static final String INCOMING_CALL_NOTIFICATION_ID = "INCOMING_CALL_NOTIFICATION_ID";
    public static final String ACTION_INCOMING_CALL = "ACTION_INCOMING_CALL";
    public static final String ACTION_FCM_TOKEN = "ACTION_FCM_TOKEN";
    String incomingCallAccessToken;
    RegistrationListener registrationListener = registrationListener();
    TabLayout tabLayout;
    public static String referralActivity = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;

        //logDeviceToken();
        Fabric.with(this, new Crashlytics());

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {

        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("No Internet Connection");
            builder.setMessage("You don't have an active internet connection.");
            builder.setCancelable(true);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                    System.exit(0);
                }
            });
            builder.create();
            builder.show();
            return;
        }

        getGuestHostDropdowns();
        getGuestsBookingInfo();

        ProgressBar pb = findViewById(R.id.progressBar);
        pb.getIndeterminateDrawable().setColorFilter(0xFFFF0000, android.graphics.PorterDuff.Mode.MULTIPLY);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        viewPager = findViewById(R.id.view_pager);
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {

                } else {

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                //Toast.makeText(MainActivity.this,"onTabUnSelected",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                //Toast.makeText(MainActivity.this,"onTabReSelected",Toast.LENGTH_SHORT).show();
            }
        });

        //Log.d("Device ID","Device ID  = "+ FirebaseInstanceId.getInstance().getToken());
        FirebaseMessaging.getInstance().subscribeToTopic("thenohotel");
        getAccessToken(getResources().getString(R.string.incomingCallIdentity));
    }

    private void getGuestsBookingInfo() {
        Utils.JSONAsyncTask guestBookingInfo = new Utils.JSONAsyncTask(new Utils.OnJSONTaskCompleted() {

            @Override
            public void onJSONTaskCompleted(JSONObject result) {
                try {
                    JSONArray cols = result.getJSONArray("DATA");
                    guestsBookingsInfos.clear();

                    for (int i =0; i < cols.length(); i++) {
                        JSONArray guestBookingInfo = cols.getJSONArray(i);
                        int bookingID = (int) guestBookingInfo.get(0);
                        String guestFirstName = guestBookingInfo.get(1).toString();
                        String guestLastName = guestBookingInfo.get(2).toString();
                        String guestPhone = guestBookingInfo.get(3).toString();
                        String property = guestBookingInfo.get(4).toString();
                        String hostFirstName = guestBookingInfo.get(5).toString();
                        String hostLastName = guestBookingInfo.get(6).toString();
                        String hostPhone = guestBookingInfo.get(7).toString();
                        String guestName = String.format("%s %s",guestFirstName,guestLastName);
                        String hostName = String.format("%s %s",hostFirstName,hostLastName);
                        String checkInDate = guestBookingInfo.get(8).toString();
                        String checkoutDate = guestBookingInfo.get(9).toString();
                        String guestPicURL = guestBookingInfo.get(10).toString();
                        guestsBookingsInfos.add(new GuestsBookingsInfo(bookingID,guestName,guestPhone,property,hostName,hostPhone,checkInDate,checkoutDate,guestPicURL));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        guestBookingInfo.execute(API_END_POINT_BOOKINGS_INFO);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"Resume Called of MainActivity");
        if (referralActivity.equals("ActiveCallActivity")) {
            tabLayout.getTabAt(1).select();
            referralActivity = "";
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        SampleAdapter adapter = new SampleAdapter(getSupportFragmentManager());
        adapter.addFragment(new SMSListFragment(),"SMS");
        adapter.addFragment(new CallLogsFragment(),"Call");
        viewPager.setAdapter(adapter);
    }

    private void getGuestHostDropdowns() {
        Utils.JSONAsyncTask guestHostDropDowns = new Utils.JSONAsyncTask(new Utils.OnJSONTaskCompleted() {

            @Override
            public void onJSONTaskCompleted(JSONObject result) {
                try {
                    JSONArray cols = result.getJSONArray("DATA");
                    guestPhones.clear();
                    hostPhones.clear();
                    guestPhones.add(new GuestPhone("Please Select Guest","","","","",0));
                    hostPhones.add(new HostPhone("Please Select Host","",0));

                    for (int i =0; i < cols.length(); i++) {
                        JSONArray guestPhonesInfo = cols.getJSONArray(i);
                        String firstName = guestPhonesInfo.get(0).toString();
                        String lastName = guestPhonesInfo.get(1).toString();
                        String checkInDate = guestPhonesInfo.get(3).toString();
                        //String checkOutDate = guestPhonesInfo.get(6).toString();
                        String guestPhone = guestPhonesInfo.get(8).toString();
                        String guestAptNo = guestPhonesInfo.get(10).toString();
                        int accountID = (int) guestPhonesInfo.get(14);
                        String guestName = String.format("%s %s",firstName,lastName);
                        String hostTwilioPhoneNumber = guestPhonesInfo.get(11).toString();
                        String hostFirstName = guestPhonesInfo.get(12).toString();
                        String hostLastName = guestPhonesInfo.get(13).toString();
                        String hostName = String.format("%s %s",hostFirstName,hostLastName);
                        String dropdownText = String.format("%s %s: %s/%s, %s", firstName,lastName, checkInDate.substring(5,7),checkInDate.substring(8,10), guestAptNo);
                        guestPhones.add(new GuestPhone(dropdownText,guestPhone,guestName,hostName,hostTwilioPhoneNumber,accountID));
                        HostPhone hostPhoneObj = new HostPhone(hostName,hostTwilioPhoneNumber,accountID);
                        if (!hostPhones.contains(hostPhoneObj)) {
                            hostPhones.add(hostPhoneObj);
                        }
                    }

                    new SMSListFragment().setSmsGuestsPhones(guestPhones);
                    new SMSListFragment().setSmsHostsPhones(hostPhones);
                    new CallLogsFragment().setVoiceGuestsPhones(guestPhones);
                    new CallLogsFragment().setVoiceHostPhones(hostPhones);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        guestHostDropDowns.execute(API_END_POINT_BOOKINGS_GUEST_HOST_DD);
    }

    public void logDeviceToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        Log.d(TAG, "Device Token:"+token);
                    }
                });
    }

    private void getAccessToken(String identity) {
        String url = String.format("%s?identity=%s",API_END_POINT_ACCESS_TOKEN,identity.trim());
        RequestQueue MyRequestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest MyStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
                incomingCallAccessToken = response;
                Log.d(TAG,"Access Token:" + incomingCallAccessToken);
                registerForCallInvites();
            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
            }
        });
        MyRequestQueue.add(MyStringRequest);
    }

    /*
     * Register your FCM token with Twilio to receive incoming call invites
     *
     * If a valid google-services.json has not been provided or the FirebaseInstanceId has not been
     * initialized the fcmToken will be null.
     *
     * In the case where the FirebaseInstanceId has not yet been initialized the
     * VoiceFirebaseInstanceIDService.onTokenRefresh should result in a LocalBroadcast to this
     * activity which will attempt registerForCallInvites again.
     *
     */
    public void registerForCallInvites() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                Log.i(TAG, "Registering with FCM");
                final String fcmToken = instanceIdResult.getToken();
                Voice.register(getApplicationContext(), incomingCallAccessToken, Voice.RegistrationChannel.FCM, fcmToken, registrationListener);
            }
        });
    }

    private RegistrationListener registrationListener() {
        return new RegistrationListener() {
            @Override
            public void onRegistered(String accessToken, String fcmToken) {
                Log.d(TAG, "Successfully registered FCM " + fcmToken);
            }

            @Override
            public void onError(RegistrationException error, String accessToken, String fcmToken) {
                String message = String.format("Registration Error: %d, %s", error.getErrorCode(), error.getMessage());
                Log.e(TAG, message);
                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
            }
        };
    }

    public static MainActivity getInstance() {
        return instance;
    }
}