package twilioapp.thenohotel.com.twiliosmsvoice.Common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class GuestSpinnerAdapter extends ArrayAdapter<GuestPhone> {
    private Context context;
    private ArrayList<GuestPhone> myGuestPhones;

    public GuestSpinnerAdapter(@NonNull Context context, int textViewResourceId, ArrayList<GuestPhone> myGuestPhones) {
        super(context, textViewResourceId, myGuestPhones);
        this.context = context;
        this.myGuestPhones = myGuestPhones;
    }


    public int getCount() {
        return myGuestPhones.size();
    }

    public GuestPhone getItem(int position) {
        return myGuestPhones.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextSize(18);
        label.setText(myGuestPhones.get(position).getText());
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextSize(18);
        label.setPadding(15,15,15,18);
        label.setText(myGuestPhones.get(position).getText());
        return label;
    }

    public HashMap<String,String> updateList(ArrayList<GuestPhone> guestPhones, int accountID, String selectedPhone) {
        int indexCtr = 0;
        HashMap<String,String> guestSelection = new HashMap<>();
        if (accountID > 0) {
            ArrayList<GuestPhone> updatedGuestPhones = new ArrayList<>();
            for (int i=0; i < guestPhones.size(); i++) {
                int hostAccountID = guestPhones.get(i).getAccountID();
                if (accountID == hostAccountID) {
                    updatedGuestPhones.add(new GuestPhone(guestPhones.get(i).getText(),guestPhones.get(i).getValue(),guestPhones.get(i).getGuestName(),guestPhones.get(i).getHostName(),guestPhones.get(i).getHostTwilioPhone(),guestPhones.get(i).getAccountID()));
                    if (guestSelection.isEmpty()) {
                        guestSelection.put("guestName",guestPhones.get(i).getGuestName());
                        guestSelection.put("guestPhone",guestPhones.get(i).getValue());
                        guestSelection.put("selectedIndex","0");
                    }
                    if (selectedPhone.equals(guestPhones.get(i).getValue())) {
                        guestSelection.clear();
                        guestSelection.put("guestName",guestPhones.get(i).getGuestName());
                        guestSelection.put("guestPhone",guestPhones.get(i).getValue());
                        guestSelection.put("selectedIndex",String.valueOf(indexCtr));
                    }
                    indexCtr++;
                }
            }
            myGuestPhones = updatedGuestPhones;
        } else {
            myGuestPhones = guestPhones;
        }
        notifyDataSetChanged();
        return guestSelection;
    }
}
