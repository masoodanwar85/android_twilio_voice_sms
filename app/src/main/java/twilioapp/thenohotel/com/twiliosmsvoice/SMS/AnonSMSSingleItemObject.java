package twilioapp.thenohotel.com.twiliosmsvoice.SMS;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AnonSMSSingleItemObject {
    private String msgFrom;
    private String hostName;
    private String hostPhone;
    private String guestPhone;
    private String message;
    private String msgTimestamp;
    private boolean isRead;

    public AnonSMSSingleItemObject(String msgFrom, String hostName,String hostPhone,String guestPhone, String message, String msgTimestamp,boolean isRead) {
        this.msgFrom = msgFrom;
        this.hostName = hostName;
        this.hostPhone = hostPhone;
        this.guestPhone = guestPhone;
        this.message = message;
        this.msgTimestamp = msgTimestamp;
        this.isRead = isRead;
    }

    public String getMsgFrom() {return msgFrom;}

    public String getHostName() {return hostName;}

    public String getHostPhone() {return hostPhone;}

    public String getGuestPhone() {return guestPhone;}

    public String getSMSMessage() {return message;}

    public String getMsgTimestamp() {
        try {
            Date SMSDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this.msgTimestamp);
            //SimpleDateFormat dtFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
            SimpleDateFormat dtFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
            return dtFormat.format(SMSDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean getIsRead() { return isRead; }
}
