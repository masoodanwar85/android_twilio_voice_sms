package twilioapp.thenohotel.com.twiliosmsvoice.SMS;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import twilioapp.thenohotel.com.twiliosmsvoice.R;
import twilioapp.thenohotel.com.twiliosmsvoice.Libraries.Utils;

import static twilioapp.thenohotel.com.twiliosmsvoice.Common.APIEndPoints.API_END_POINT_ANON_THREAD_SMSes;
import static twilioapp.thenohotel.com.twiliosmsvoice.Common.APIEndPoints.API_END_POINT_SEND_MESSAGE;

public class AnonSMSDetailActivity extends AppCompatActivity {
    ArrayList<AnonSMSSingleItemObject> threadSMSes;
    RecyclerView singleThreadAnonSMSes;
    ProgressBar pbDetailAnonMessage;
    ImageView btnAnonDetailSend;
    public static String toPhoneNumber;
    public static String fromPhoneNumber;
    EditText anonMessageToSend;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.anon_sms_detail_activity);

        Intent intent = getIntent();
        String hostPhone = intent.getStringExtra("com.thenohotel.twilioapp.hostNumber");
        String guestPhone = intent.getStringExtra("com.thenohotel.twilioapp.guestNumber");


        if (guestPhone == null && hostPhone == null) {
            hostPhone = getIntent().getExtras().get("hostNumber").toString();
            guestPhone = getIntent().getExtras().get("guestNumber").toString();
        }

        toPhoneNumber = guestPhone;
        fromPhoneNumber = hostPhone;
        android.support.v7.app.ActionBar ab = getSupportActionBar();

        ab.setTitle(String.format("Guest: %s",Utils.formatPhone(guestPhone)));
        String url = String.format("%s?hostPhone=%s&guestPhone=%s",API_END_POINT_ANON_THREAD_SMSes,hostPhone,guestPhone);

        Utils.JSONAsyncTask anonThreadSMSesJSON = new Utils.JSONAsyncTask(new Utils.OnJSONTaskCompleted() {
            @Override
            public void onJSONTaskCompleted(JSONObject result) {
                try {
                    JSONArray cols = result.getJSONArray("DATA");
                    threadSMSes = new ArrayList<>();
                    for (int i =0; i < cols.length(); i++) {
                        JSONArray twilioSMSThread = cols.getJSONArray(i);
                        String smsMessage = twilioSMSThread.get(2).toString();
                        String smsTimestamp = twilioSMSThread.get(4).toString();
                        String smsFrom = twilioSMSThread.get(6).toString();
                        String fromPhoneNumber = twilioSMSThread.get(0).toString();
                        String toPhoneNumber = twilioSMSThread.get(1).toString();
                        String hostName = twilioSMSThread.get(7).toString() + " " + twilioSMSThread.get(8).toString();
                        String guestPhone = fromPhoneNumber;
                        String hostPhone = toPhoneNumber;
                        if (smsFrom.equals("Host")) {
                            guestPhone = toPhoneNumber;
                            hostPhone = fromPhoneNumber;
                        }
                        threadSMSes.add(new AnonSMSSingleItemObject(smsFrom,hostName,hostPhone,guestPhone,smsMessage,smsTimestamp,true));
                        singleThreadAnonSMSes.setAdapter(new AnonSMSDetailAdapter(threadSMSes));
                        //progressBar.setVisibility(getView().INVISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        anonThreadSMSesJSON.execute(url);

        anonMessageToSend = findViewById(R.id.anonDetailMessage);
        btnAnonDetailSend = findViewById(R.id.btnAnonDetailSend);
        pbDetailAnonMessage = findViewById(R.id.pbAnonDetailMessage);
        pbDetailAnonMessage.getIndeterminateDrawable().setColorFilter(0xFFFF0000, android.graphics.PorterDuff.Mode.MULTIPLY);
        singleThreadAnonSMSes = findViewById(R.id.reyclerview_anon_message_list);
        singleThreadAnonSMSes.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(singleThreadAnonSMSes.getContext(),DividerItemDecoration.VERTICAL);
        singleThreadAnonSMSes.addItemDecoration(dividerItemDecoration);

        btnAnonDetailSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (anonMessageToSend.getText().toString().trim().equals("")) {
                    Toast.makeText(getApplicationContext(),"Please type the message.",Toast.LENGTH_SHORT).show();
                } else {
                    pbDetailAnonMessage.setVisibility(View.VISIBLE);
                    String url = API_END_POINT_SEND_MESSAGE;
                    RequestQueue MyRequestQueue = Volley.newRequestQueue(getApplicationContext());
                    StringRequest MyStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                            //"Host",anonMessageToSend.getText().toString().trim(),timestamp.toString())
                            threadSMSes.add(threadSMSes.size(),new AnonSMSSingleItemObject("Host","",fromPhoneNumber,toPhoneNumber,anonMessageToSend.getText().toString().trim(),timestamp.toString(),true));
                            AnonSMSDetailAdapter anonSMSDetailAdapter = new AnonSMSDetailAdapter(threadSMSes);
                            anonSMSDetailAdapter.notifyItemInserted(threadSMSes.size()-1);
                            singleThreadAnonSMSes.requestLayout();
                            anonMessageToSend.setText("");
                            // Add the current message to the SMS Detail List
                            // Update the SMSListAdapter so that it shows the latest message
                            Toast.makeText(getApplicationContext(),"Message was sent successfully.",Toast.LENGTH_SHORT).show();
                            pbDetailAnonMessage.setVisibility(View.GONE);

                        }
                    }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //This code is executed if there is an error.
                        }
                    }) {
                        protected Map<String, String> getParams() {
                            Map<String, String> MyData = new HashMap<String, String>();
                            MyData.put("toNumber",toPhoneNumber);
                            MyData.put("fromNumber",fromPhoneNumber);
                            MyData.put("message",anonMessageToSend.getText().toString());
                            return MyData;
                        }
                    };
                    MyRequestQueue.add(MyStringRequest);

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            this.finish();
        } else {
            super.onBackPressed(); //replaced
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent parentIntent = NavUtils.getParentActivityIntent(this);
                if(parentIntent == null) {
                    finish();
                    return true;
                } else {
                    parentIntent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(parentIntent);
                    finish();
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }
}
