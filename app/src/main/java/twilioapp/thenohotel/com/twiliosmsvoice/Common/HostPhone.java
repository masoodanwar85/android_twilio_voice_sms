package twilioapp.thenohotel.com.twiliosmsvoice.Common;

public class HostPhone {
    String hostName;
    String hostTwilioPhone;
    int accountID;

    public HostPhone(String hostName, String hostTwilioPhone, int accountID) {
        this.hostName = hostName;
        this.hostTwilioPhone = hostTwilioPhone;
        this.accountID = accountID;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getHostTwilioPhone() {
        return hostTwilioPhone;
    }

    public void setHostTwilioPhone(String hostTwilioPhone) {
        this.hostTwilioPhone = hostTwilioPhone;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    @Override
    public boolean equals(Object obj) {
        return this.accountID == ((HostPhone) obj).getAccountID();
    }
}
