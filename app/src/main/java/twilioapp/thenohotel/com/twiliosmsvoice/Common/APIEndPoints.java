package twilioapp.thenohotel.com.twiliosmsvoice.Common;

public class APIEndPoints {
    public static final String API_END_POINT_BASE_URL = "http://thenohotel.com/twilio";
    public static final String API_END_POINT_ACCESS_TOKEN = String.format("%s/voice/getAccessToken.cfm",API_END_POINT_BASE_URL);
    public static final String API_END_POINT_SEND_MESSAGE = String.format("%s/sms/mobileSendMessage.cfm",API_END_POINT_BASE_URL);
    public static final String API_END_POINT_SMS_THREADS = String.format("%s/sms/getSMSThreadsJSON.cfm",API_END_POINT_BASE_URL);
    public static final String API_END_POINT_BOOKINGS_INFO = String.format("%s/common/getBookingsJSON.cfm",API_END_POINT_BASE_URL);
    public static final String API_END_POINT_BOOKINGS_GUEST_HOST_DD = String.format("%s/common/getBookingGuestsJSON.cfm",API_END_POINT_BASE_URL);
    public static final String API_END_POINT_ANON_THREAD_SMSes = String.format("%s/sms/getAnonThreadsSMSesJSON.cfm",API_END_POINT_BASE_URL);
    public static final String API_END_POINT_THREAD_SMSes = String.format("%s/sms/getThreadSMSesJSON.cfm",API_END_POINT_BASE_URL);
    public static final String API_END_POINT_CALL_LOG_DETAIL = String.format("%s/voice/getCallLogDetailsJSON.cfm",API_END_POINT_BASE_URL);
    public static final String API_END_POINT_CALL_LOGS = String.format("%s/voice/getCallLogThreadsJSON.cfm",API_END_POINT_BASE_URL);
    public static final String API_END_POINT_DELETE_SMS = String.format("%s/sms/actDeleteThread.cfm",API_END_POINT_BASE_URL);
}
