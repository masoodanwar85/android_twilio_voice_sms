package twilioapp.thenohotel.com.twiliosmsvoice.Voice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CallLogDetailsObject {
    private String fromPhone;
    private String toPhone;
    private String dateCreated;
    private String fromToPhone;
    private String hostName;
    private String guestName;
    private String hostPhoneNumber;
    private String guestPhoneNumber;

    public CallLogDetailsObject(String fromPhone, String toPhone, String dateCreated, String fromToPhone, String hostName, String guestName, String hostPhoneNumber, String guestPhoneNumber) {
        this.fromPhone = fromPhone;
        this.toPhone = toPhone;
        this.dateCreated = dateCreated;
        this.fromToPhone = fromToPhone;
        this.hostName = hostName;
        this.guestName = guestName;
        this.hostPhoneNumber = hostPhoneNumber;
        this.guestPhoneNumber = guestPhoneNumber;
    }

    public String getFromPhone() {
        return fromPhone;
    }

    public void setFromPhone(String fromPhone) {
        this.fromPhone = fromPhone;
    }

    public String getToPhone() {
        return toPhone;
    }

    public void setToPhone(String toPhone) {
        this.toPhone = toPhone;
    }

    public String getDateCreated() {
        try {
            Date SMSDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this.dateCreated);
            //SimpleDateFormat dtFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
            SimpleDateFormat dtFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
            return dtFormat.format(SMSDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getFromToPhone() {
        return fromToPhone;
    }

    public void setFromToPhone(String fromToPhone) {
        this.fromToPhone = fromToPhone;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getHostPhoneNumber() {
        return hostPhoneNumber;
    }

    public void setHostPhoneNumber(String hostPhoneNumber) {
        this.hostPhoneNumber = hostPhoneNumber;
    }

    public String getGuestPhoneNumber() {
        return guestPhoneNumber;
    }

    public void setGuestPhoneNumber(String guestPhoneNumber) {
        this.guestPhoneNumber = guestPhoneNumber;
    }
}

