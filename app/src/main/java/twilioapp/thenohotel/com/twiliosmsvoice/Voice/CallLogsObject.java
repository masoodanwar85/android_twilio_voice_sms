package twilioapp.thenohotel.com.twiliosmsvoice.Voice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CallLogsObject {
    private int bookingID;
    private String callSID;
    private String callStatus;
    private int duration;
    private String direction;
    private String fromPhone;
    private String toPhone;
    private String dateCreated;
    private String fromToPhone;
    private String hostName;
    private String guestName;
    private String hostPhoneNumber;
    private String guestPhoneNumber;

    public CallLogsObject(int bookingID, String callSID, String callStatus, int duration, String direction, String fromPhone, String toPhone, String dateCreated, String fromToPhone, String hostName, String guestName, String hostPhoneNumber, String guestPhoneNumber) {
        this.bookingID = bookingID;
        this.callSID = callSID;
        this.callStatus = callStatus;
        this.duration = duration;
        this.direction = direction;
        this.fromPhone = fromPhone;
        this.toPhone = toPhone;
        this.dateCreated = dateCreated;
        this.fromToPhone = fromToPhone;
        this.hostName = hostName;
        this.guestName = guestName;
        this.hostPhoneNumber = hostPhoneNumber;
        this.guestPhoneNumber = guestPhoneNumber;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public String getCallSID() {
        return callSID;
    }

    public void setCallSID(String callSID) {
        this.callSID = callSID;
    }

    public String getCallStatus() {
        return callStatus;
    }

    public void setCallStatus(String callStatus) {
        this.callStatus = callStatus;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getFromPhone() {
        return fromPhone;
    }

    public void setFromPhone(String fromPhone) {
        this.fromPhone = fromPhone;
    }

    public String getToPhone() {
        return toPhone;
    }

    public void setToPhone(String toPhone) {
        this.toPhone = toPhone;
    }

    public String getDateCreated() {
        try {
            Date SMSDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this.dateCreated);
            //SimpleDateFormat dtFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
            SimpleDateFormat dtFormat = new SimpleDateFormat("MMM dd, hh:mm a");
            return dtFormat.format(SMSDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getFromToPhone() {
        return fromToPhone;
    }

    public void setFromToPhone(String fromToPhone) {
        this.fromToPhone = fromToPhone;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getHostPhoneNumber() {
        return hostPhoneNumber;
    }

    public void setHostPhoneNumber(String hostPhoneNumber) {
        this.hostPhoneNumber = hostPhoneNumber;
    }

    public String getGuestPhoneNumber() {
        return guestPhoneNumber;
    }

    public void setGuestPhoneNumber(String guestPhoneNumber) {
        this.guestPhoneNumber = guestPhoneNumber;
    }
}
