package twilioapp.thenohotel.com.twiliosmsvoice.SMS;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SMSListObject {
    private boolean isRead;
    private int threadID;
    private int bookingID;
    private String lastSMS;
    private String lastSMSTimestamp;
    private String guestPicURL;
    private String fromPhoneNumber;
    private String toPhoneNumber;
    private String guestName;
    private String twilioPhoneNumber;
    private String msgFrom;
    private String hostGuestNumber;
    private String hostPhoneNumber;
    private String guestPhoneNumber;
    private String hostName;

    public SMSListObject(boolean isRead, int bookingID, int threadID, String lastSMS, String lastSMSTimestamp, String guestPicURL, String fromPhoneNumber, String toPhoneNumber, String guestName, String twilioPhoneNumber, String msgFrom, String hostGuestNumber, String hostName,String hostPhoneNumber,String guestPhoneNumber) {
        this.isRead = isRead;
        this.bookingID = bookingID;
        this.threadID = threadID;
        this.lastSMS = lastSMS;
        this.lastSMSTimestamp = lastSMSTimestamp;
        this.guestPicURL = guestPicURL;
        this.fromPhoneNumber = fromPhoneNumber;
        this.toPhoneNumber = toPhoneNumber;
        this.guestName = guestName;
        this.twilioPhoneNumber = twilioPhoneNumber;
        this.msgFrom = msgFrom;
        this.hostGuestNumber = hostGuestNumber;
        this.hostName = hostName;
        this.hostPhoneNumber = hostPhoneNumber;
        this.guestPhoneNumber = guestPhoneNumber;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(boolean read) {
        isRead = read;
    }

    public int getThreadID() {
        return threadID;
    }

    public void setThreadID(int threadID) {
        this.threadID = threadID;
    }

    public String getGuestPicURL() {
        return guestPicURL;
    }

    public void setGuestPicURL(String guestPicURL) {
        this.guestPicURL = guestPicURL;
    }

    public String getFromPhoneNumber() {
        return fromPhoneNumber;
    }

    public void setFromPhoneNumber(String fromPhoneNumber) {
        this.fromPhoneNumber = fromPhoneNumber;
    }

    public String getToPhoneNumber() {
        return toPhoneNumber;
    }

    public void setToPhoneNumber(String toPhoneNumber) {
        this.toPhoneNumber = toPhoneNumber;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getTwilioPhoneNumber() {
        return twilioPhoneNumber;
    }

    public void setTwilioPhoneNumber(String twilioPhoneNumber) {
        this.twilioPhoneNumber = twilioPhoneNumber;
    }

    public String getMsgFrom() {
        return msgFrom;
    }

    public void setMsgFrom(String msgFrom) {
        this.msgFrom = msgFrom;
    }

    public String getHostGuestNumber() {
        return hostGuestNumber;
    }

    public void setHostGuestNumber(String hostGuestNumber) {
        this.hostGuestNumber = hostGuestNumber;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getLastSMS() {
        if (lastSMS.length() > 120) {
            return lastSMS.substring(0,120).concat("...");
        } else {
            return lastSMS;
        }
    }

    public void setLastSMS(String lastSMS) {
        this.lastSMS = lastSMS;
    }

    public String getLastSMSTimestamp() {
        try {
            Date SMSDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this.lastSMSTimestamp);
            //SimpleDateFormat dtFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
            SimpleDateFormat dtFormat = new SimpleDateFormat("MMM dd, yyyy");
            return dtFormat.format(SMSDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setLastSMSTimestamp(String lastSMSTimestamp) {
        this.lastSMSTimestamp = lastSMSTimestamp;
    }

    public String getHostPhoneNumber() {
        return hostPhoneNumber;
    }

    public void setHostPhoneNumber(String hostPhoneNumber) {
        this.hostPhoneNumber = hostPhoneNumber;
    }

    public String getGuestPhoneNumber() {
        return guestPhoneNumber;
    }

    public void setGuestPhoneNumber(String guestPhoneNumber) {
        this.guestPhoneNumber = guestPhoneNumber;
    }
}
