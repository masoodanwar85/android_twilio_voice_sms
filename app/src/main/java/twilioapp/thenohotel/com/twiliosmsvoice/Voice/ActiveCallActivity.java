package twilioapp.thenohotel.com.twiliosmsvoice.Voice;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Build;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.twilio.voice.Call;
import com.twilio.voice.CallException;
import com.twilio.voice.CallInvite;
import com.twilio.voice.Voice;

import java.util.HashMap;

import twilioapp.thenohotel.com.twiliosmsvoice.Common.MainActivity;
import twilioapp.thenohotel.com.twiliosmsvoice.Libraries.SoundPoolManager;
import twilioapp.thenohotel.com.twiliosmsvoice.Libraries.Utils;
import twilioapp.thenohotel.com.twiliosmsvoice.R;

import static twilioapp.thenohotel.com.twiliosmsvoice.Common.APIEndPoints.API_END_POINT_ACCESS_TOKEN;

//https://media.twiliocdn.com/sdk/android/voice/releases/2.0.8/docs/
public class ActiveCallActivity extends AppCompatActivity {
    private static String TAG = "MyActiveCallActivity";
    HashMap<String, String> twiMLParams = new HashMap<>();
    private Call activeCall;
    private CallInvite activeCallInvite;
    private FloatingActionButton hangupActionFab;
    private FloatingActionButton muteActionFab;
    private Chronometer chronometer;
    TextView accessTokenRetrieval;
    TextView activeCallStatusText;
    TextView activeCallGuestName;
    TextView activeCallGuestNumber;
    private static AudioManager audioManager;
    public static SoundPoolManager soundPoolManager;
    private static int savedAudioMode = AudioManager.MODE_INVALID;
    Call.Listener callListener = callListener();
    Context context;

    private AlertDialog alertDialog;
    private NotificationManager notificationManager;

    private int activeCallNotificationId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        context = getApplicationContext();
        hangupActionFab = findViewById(R.id.hangup_action_fab);
        muteActionFab = findViewById(R.id.mute_action_fab);
        chronometer = findViewById(R.id.chronometer);
        accessTokenRetrieval = findViewById(R.id.msgAccessToken);
        activeCallStatusText = findViewById(R.id.activeCallStatus);
        activeCallGuestName = findViewById(R.id.activeCallGuestName);
        activeCallGuestNumber = findViewById(R.id.activeCallGuestNumber);

        hangupActionFab.setOnClickListener(hangupActionFabClickListener());
        muteActionFab.setOnClickListener(muteActionFabClickListener());
        soundPoolManager = SoundPoolManager.getInstance(getApplicationContext());
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setSpeakerphoneOn(false);

        setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
        soundPoolManager = SoundPoolManager.getInstance(this);

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        /*
         * Setup the broadcast receiver to be notified of FCM Token updates
         * or incoming call invite in this Activity.
         */

        String accessToken = null;
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        String callToPhoneNumber = extras.getString("com.thenohotel.twilioapp.voice.CALL_TO");
        String callToGuestName = extras.getString("com.thenohotel.twilioapp.voice.CALL_TO_GUEST_NAME");
        String callFromPhoneNumber = extras.getString("com.thenohotel.twilioapp.voice.CALL_FROM");
        twiMLParams.put("to", callToPhoneNumber);

        setActiveCallGuestInfo(callToGuestName,callToPhoneNumber);

        if (extras.containsKey("com.thenohotel.twilioapp.voice.ACCESS_TOKEN")) {
            accessToken = extras.getString("com.thenohotel.twilioapp.voice.ACCESS_TOKEN");
            initiateCall(accessToken);
        } else {
            accessTokenRetrieval.setVisibility(View.VISIBLE);
            getAccessToken(callFromPhoneNumber);
        }
    }

    private void initiateCall(String accessToken) {
        activeCall = Voice.call(getApplicationContext(), accessToken, twiMLParams, callListener);
        setCallUI();
        setActiveCallStatus("Dialing...");
    }

    private void setActiveCallStatus(String defaultText) {
        activeCallStatusText.setText(defaultText);
        if ((activeCallInvite != null) && (!activeCallInvite.getState().equals(""))) {
            activeCallStatusText.setText(activeCallInvite.getState().toString());
        }
        activeCallStatusText.setVisibility(View.VISIBLE);
    }

    private void setActiveCallGuestInfo(String guestName,String guestNumber) {
        activeCallGuestNumber.setText(guestNumber);
        activeCallGuestNumber.setVisibility(View.VISIBLE);
        if (guestName != null && !guestName.equals("")) {
            activeCallGuestName.setText(guestName);
            activeCallGuestName.setVisibility(View.VISIBLE);
        }
    }

    private Call.Listener callListener() {
        return new Call.Listener() {
            @Override
            public void onConnectFailure(Call call, CallException error) {
                setAudioFocus(false);
                Log.d(TAG, "Connect failure");
                String message = String.format("Call Error: %d, %s", error.getErrorCode(), error.getMessage());
                Log.e(TAG, message);
                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                resetUI();
            }

            @Override
            public void onConnected(Call call) {
                setAudioFocus(true);
                Log.d(TAG, "Connected");
                activeCall = call;
                startChronometer();
                setActiveCallStatus("Call Connected");
            }

            @Override
            public void onDisconnected(Call call, CallException error) {
                setAudioFocus(false);
                Log.d(TAG, "Disconnected");
                if (error != null) {
                    String message = String.format("Call Error: %d, %s", error.getErrorCode(), error.getMessage());
                    Log.e(TAG, message);
                    Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                }
                resetUI();
                finish();
            }
        };
    }




    private View.OnClickListener hangupActionFabClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Hangup Action");
                soundPoolManager.playDisconnect();
                resetUI();
                disconnect();
            }
        };
    }

    /*
     * Disconnect from Call
     */
    private void disconnect() {
        Log.d(TAG, "Disconnected");
        if (activeCall != null) {
            activeCall.disconnect();
            activeCall = null;
        }
        finish();
    }

    private void mute() {
        if (activeCall != null) {
            boolean mute = !activeCall.isMuted();
            activeCall.mute(mute);
            if (mute) {
                muteActionFab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_mic_white_off_24dp));
            } else {
                muteActionFab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_mic_white_24dp));
            }
        }
    }

    private View.OnClickListener muteActionFabClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mute();
            }
        };
    }

    /*
     * The UI state when there is an active call
     */
    private void setCallUI() {
        hangupActionFab.show();
        muteActionFab.show();
    }

    /*
     * Reset UI elements
     */
    private void resetUI() {
        muteActionFab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_mic_white_24dp));
        muteActionFab.hide();
        hangupActionFab.hide();
        chronometer.setVisibility(View.INVISIBLE);
        chronometer.stop();

        activeCallStatusText.setText("");
        activeCallGuestName.setText("");
        activeCallGuestNumber.setText("");
        activeCallStatusText.setVisibility(View.INVISIBLE);
        activeCallGuestName.setVisibility(View.INVISIBLE);
        activeCallGuestNumber.setVisibility(View.INVISIBLE);
    }

    private void startChronometer() {
        chronometer.setVisibility(View.VISIBLE);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        soundPoolManager.release();
        super.onDestroy();
    }

    private void getAccessToken(String identity) {
        String url = String.format("%s?identity=%s",API_END_POINT_ACCESS_TOKEN,identity.trim());
        RequestQueue MyRequestQueue = Volley.newRequestQueue(context);
        StringRequest MyStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //This code is executed if the server responds, whether or not the response contains data.
                //The String 'response' contains the server's response.
                accessTokenRetrieval.setVisibility(View.INVISIBLE);
                initiateCall(response);
            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
            }
        });
        MyRequestQueue.add(MyStringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.speaker_menu_item:
                if (audioManager.isSpeakerphoneOn()) {
                    audioManager.setSpeakerphoneOn(false);
                    item.setIcon(R.drawable.ic_phonelink_ring_white_24dp);
                } else {
                    audioManager.setSpeakerphoneOn(true);
                    item.setIcon(R.drawable.ic_volume_up_white_24dp);
                }
                break;
        }
        return true;
    }

    public static void setAudioFocus(boolean setFocus) {
        if (audioManager != null) {
            if (setFocus) {
                savedAudioMode = audioManager.getMode();
                // Request audio focus before making any device switch.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    AudioAttributes playbackAttributes = new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION)
                            .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                            .build();
                    AudioFocusRequest focusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                            .setAudioAttributes(playbackAttributes)
                            .setAcceptsDelayedFocusGain(true)
                            .setOnAudioFocusChangeListener(new AudioManager.OnAudioFocusChangeListener() {
                                @Override
                                public void onAudioFocusChange(int i) {
                                }
                            })
                            .build();
                    audioManager.requestAudioFocus(focusRequest);
                } else {
                    audioManager.requestAudioFocus(null, AudioManager.STREAM_VOICE_CALL,
                            AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                }
                /*
                 * Start by setting MODE_IN_COMMUNICATION as default audio mode. It is
                 * required to be in this mode when playout and/or recording starts for
                 * best possible VoIP performance. Some devices have difficulties with speaker mode
                 * if this is not set.
                 */
                audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            } else {
                audioManager.setMode(savedAudioMode);
                audioManager.abandonAudioFocus(null);
            }
        }
    }


    private DialogInterface.OnClickListener cancelCallClickListener() {
        return new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                soundPoolManager.stopRinging();
                Log.d(TAG, "Call Rejected");
                if (activeCallInvite != null) {
                    activeCallInvite.reject(ActiveCallActivity.this);
                    Log.d(TAG,"Cancel Call Notification ID = " + activeCallNotificationId);
                    notificationManager.cancel(activeCallNotificationId);
                    Log.d(TAG,"if activeCallInvite != null condition called...");
                }
                Log.d(TAG,"after if condition called.");
                alertDialog.dismiss();
                MainActivity.referralActivity = "ActiveCallActivity";
                //ActiveCallActivity.this.finish();

                //TODO: When rejected, ActiveCallActivity comes back to front after MainActivity (Bug)

            }
        };
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d(TAG,"onNewIntent Called...");
    }
}